<?php

namespace Drupal\translation_outdated;

use Drupal\content_translation\ContentTranslationHandler;
use Drupal\content_translation\ContentTranslationHandlerInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\translation_outdated\Form\TranslationOutdatedConfirm;

/**
 * Base class for content translation handlers.
 *
 * @ingroup entity_api
 */
class CustomContentTranslationHandler extends ContentTranslationHandler implements ContentTranslationHandlerInterface, EntityHandlerInterface {
  use StringTranslationTrait;
  /**
   * {@inheritdoc}
   */
  public function entityFormAlter(array &$form, FormStateInterface $form_state, EntityInterface $entity) {
    parent::entityFormAlter($form, $form_state, $entity);
    unset($form['content_translation']['created']);

    $account = \Drupal::currentUser();
    $items = ['outdated', 'retranslate', 'retranslate_child'];
    foreach ($items as $item) {
      unset($form['content_translation'][$item]);
    }
    $source_langcode = $this->getSourceLangcode($form_state);
    $new_translation = !empty($source_langcode);
    $metadata = $this->manager->getTranslationMetadata($entity);

    if (!$new_translation) {
      $mark_one = $account->hasPermission('mark one translation as outdated');
      $mark_all = $account->hasPermission('bulk mark translations as outdated');
      $form['content_translation']['outdated'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('This translation needs to be updated'),
        '#default_value' => !$new_translation && $metadata->isOutdated(),
        '#description'   => $this->t('When this option is checked, this translation needs to be updated. Uncheck when the translation is up to date again.'),
        '#access'        => $mark_one,
      ];

      $form['content_translation']['bulk_update'] = [
        '#type'         => 'fieldset',
        '#title'        => $this->t('Bulk update translations as outdated'),
        '#description'  => $this->t('If you made a significant change, which means the several other translations should be updated, you can flag them as outdated. This will not change any other property of them, like whether they are published or not.'),
      ];

      $form['content_translation']['bulk_update']['retranslate'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Flag all other translations as outdated'),
        '#default_value' => FALSE,
        '#access'        => $mark_all,
      ];
      $form['content_translation']['bulk_update']['retranslate_child'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Flag translations using this language as source as outdated'),
        '#default_value' => FALSE,
        '#access'        => $mark_all,
      ];
      $form['content_translation']['#open'] = $new_translation || $metadata->isOutdated();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function entityFormSubmit($form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityFormInterface $form_object */
    $form_object = $form_state->getFormObject();
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_object->getEntity();
    /** @var array $translations */
    $translations = $form_state->getValue('content_translation');
    // Handle "outdated" checkbox.
    if ($translations['outdated']) {
      $langcode = $entity->language()->getId();
      if (!TranslationOutdatedConfirm::isOutdated($langcode, $entity->id())) {
        \Drupal::service('content_translation.manager')
          ->getTranslationMetadata($entity->getTranslation($langcode))
          ->setOutdated(TRUE);
      }
    }
    if ($translations['bulk_update']['retranslate'] || $translations['bulk_update']['retranslate_child']) {
      // Store the form and form state data into the user private tempstore.
      /** @var \Drupal\user\PrivateTempStoreFactory $storage */
      $storage = \Drupal::service('user.private_tempstore');
      $collection = $storage->get('translation_outdated_storage');
      $collection->set('entity', $entity);
      $collection->set('return_path', \Drupal::request()->getRequestUri());
      $collection->set('translation_fields', $translations['bulk_update']);
      $redirect = Url::fromRoute('translation_outdated.bulk_confirm');
      // Something blocks us from using $form_state->setRedirect().
      // Probably something related to the parent classes or form structure.
      header('Location: /' . $redirect->getInternalPath()); exit;
    }
  }

}
