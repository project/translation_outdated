<?php

namespace Drupal\translation_outdated\Form;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

/**
 * Class TranslationOutdatedConfirm.
 *
 * @package Drupal\translation_outdated\Form
 */
class TranslationOutdatedConfirm extends ConfirmFormBase {
  /**
   * Cancel URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $cancelUrl;
  /**
   * User private temp storage.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $storage;
  /**
   * Content translation manager service.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $manager;
  /**
   * Languages for "retranslate child" process.
   *
   * @var array
   */
  protected $retranslateChildrenLanguages = [];
  /**
   * Languages for "retranslate" process.
   *
   * @var array
   */
  protected $retranslateLanguages = [];
  /**
   * Processed node entity.
   *
   * @var Node
   */
  protected $entity;

  /**
   * TranslationOutdatedConfirm constructor.
   */
  public function __construct() {
    $this->storage = \Drupal::service('user.private_tempstore');
    $this->manager = \Drupal::service('content_translation.manager');

    $this->cancelUrl = Url::fromUri(
      'base:/' . $this->getPrivateStorage()->get('return_path'),
      [
        'language' => \Drupal::languageManager()->getCurrentLanguage(),
        'absolute' => TRUE,
      ]
    );

    $collection   = $this->getPrivateStorage();
    $this->entity = $collection->get('entity');

    /** @var \Drupal\content_translation\ContentTranslationMetadataWrapperInterface $metadata */
    $metadata = $this->getTranslationMetadataWrapper($this->entity);
    if ($this->entity->hasField('content_translation_changed')) {
      $metadata->setChangedTime(\Drupal::time()->getRequestTime());
    }
    $values = $collection->get('translation_fields');
    if (!empty($values['retranslate'])) {
      $updated_langcode = $this->entity->language()->getId();
      foreach ($this->entity->getTranslationLanguages() as $langcode => $language) {
        if ($langcode != $updated_langcode && !self::isOutdated($langcode, $this->entity->id())) {
          $this->retranslateLanguages[] = $langcode;
        }
      }
    }
    if (!empty($values['retranslate_child'])) {
      $languages       = [];
      $source_langcode = $this->entity->language()->getId();
      foreach ($this->entity->getTranslationLanguages() as $langcode => $language) {
        $this->getChild($this->entity->getTranslation($langcode), $source_langcode, $languages);
      }
      if (!empty($languages)) {
        foreach (array_keys($languages) as $langcode) {
          if (!self::isOutdated($langcode, $this->entity->id()) && $langcode != $source_langcode) {
            $this->retranslateChildrenLanguages[] = $langcode;
          }
        }
      }
    }
  }

  /**
   * Get child helper method.
   *
   * @param ContentEntityInterface $translation
   *   Translation to be used for getting children.
   * @param string $source_langcode
   *   Source langcode.
   * @param array &$children
   *   Children array.
   */
  protected function getChild(ContentEntityInterface $translation, $source_langcode, &$children) {
    $source = $translation->get('content_translation_source')->getValue()[0]['value'];
    if (!empty($source) && $source != Language::LANGCODE_NOT_SPECIFIED) {
      $children[$translation->language()->getId()] = $source;
      $this->getChild($translation->getTranslation($source), $source_langcode, $children);
    }
  }

  /**
   * Check if translation is marked as outdated.
   *
   * @param string $langcode
   *   Langcode.
   * @param string|int $entity_id
   *   Entity ID.
   *
   * @return bool
   *   TRUE - if outdated, FALSE otherwise.
   */
  public static function isOutdated($langcode, $entity_id) {
    $select = \Drupal::database()
      ->select('node_field_data', 'n');
    $select->fields('n', ['content_translation_outdated']);
    $select->condition('nid', $entity_id);
    $select->condition('langcode', $langcode);
    $field = $select->execute()->fetchField();
    return $field > 0;
  }

  /**
   * Get private storage data collection.
   *
   * @return \Drupal\user\PrivateTempStore
   *   Private storage data collection.
   */
  protected function getPrivateStorage() {
    return $this->storage->get('translation_outdated_storage');
  }

  /**
   * Returns the question to ask the user.
   *
   * @return \Drupal\Core\StringTranslation\PluralTranslatableMarkup
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->getStringTranslation()
      ->formatPlural(
        count($this->getAllProcessedLanguages()),
        'Are you sure you wish to mark 1 translation as outdated',
        'Are you sure you wish to mark @count translations as outdated'
      );
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return $this->cancelUrl;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'translation_outdated_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $languages = $this->getAllProcessedLanguages();
    // Show message if no languages will be updated.
    if (empty($languages)) {
      $form['description']['#markup'] = $this->t('There are no languages will be marked as outdated');
      unset($form['actions']['submit']);
      $form['actions']['cancel']['#title'] = $this->t('Go back');
      return $form;
    }
    $form['description']['#markup'] = $this->t('These languages will be marked as outdated:');
    $form['languages'] = [
      '#type'   => '#markup',
      '#markup' => implode('</li><li>', $languages),
      '#prefix' => '<ul><li>',
      '#suffix' => '</li></ul>',
    ];
    return $form;
  }

  /**
   * Get all processed languages comma-separated list.
   */
  protected function getAllProcessedLanguages() {
    $languages = [];
    if (!empty($this->retranslateLanguages)) {
      foreach ($this->retranslateLanguages as $langcode) {
        // Prevent duplicates.
        if (!isset($languages[$langcode])) {
          $languages[$langcode] = \Drupal::languageManager()
            ->getLanguage($langcode)
            ->getName();
        }
      }
    }
    if (!empty($this->retranslateChildrenLanguages)) {
      foreach ($this->retranslateChildrenLanguages as $langcode) {
        // Prevent duplicates.
        if (!isset($languages[$langcode])) {
          $languages[$langcode] = \Drupal::languageManager()
            ->getLanguage($langcode)
            ->getName();
        }
      }
    }
    return $languages;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($this->retranslateLanguages)) {
      $this->retranslate($this->retranslateLanguages);
    }
    if (!empty($this->retranslateChildrenLanguages)) {
      $this->retranslate($this->retranslateChildrenLanguages);
    }
    $this->entity->save();
    // Cleanup temp storage.
    $collection = $this->getPrivateStorage();
    $collection->delete('entity');
    $collection->delete('return_path');
    $collection->delete('translation_fields');
    // Redirect back to the edit form.
    $form_state->setRedirectUrl($this->getCancelUrl());
    $this->messenger()->addStatus($this->t('Outdated translations have been marked'));
    return;
  }

  /**
   * Re-translation handler.
   *
   * @param array $languages
   *   Array of langcodes which needs to be marked as outdated.
   */
  protected function retranslate(array $languages = []) {
    if (!empty($languages)) {
      foreach ($languages as $langcode) {
        $this->getTranslationMetadataWrapper($this->entity, $langcode)
          ->setOutdated(TRUE);
      }
    }
  }

  /**
   * Get translation metadata wrapper.
   *
   * @param \Drupal\node\Entity\Node $entity
   *   Entity object.
   * @param string|null $langcode
   *   Language code.
   *
   * @return \Drupal\content_translation\ContentTranslationMetadataWrapperInterface
   *   Translation metadata wrapper.
   */
  protected function getTranslationMetadataWrapper(Node $entity, $langcode = NULL) {
    $langcode = !empty($langcode) ? $langcode : $entity->language()->getId();
    return $this->manager
      ->getTranslationMetadata($entity->getTranslation($langcode));
  }

}
